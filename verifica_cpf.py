class VerificaCPF(object):

    def __init__(self, cpf):
        self.cpf = ''.join(x for x in cpf if x.isdigit())

    def validar(self, d1=0, d2=0, i=0):
        while i < 10:
            d1, d2, i = (d1 + (int(self.cpf[i]) * (11 - i - 1))) % 11 if i < 9 else d1, (
                        d2 + (int(self.cpf[i]) * (11 - i))) % 11, i + 1
        return (int(self.cpf[9]) == (11 - d1 if d1 > 1 else 0)) and (int(self.cpf[10]) == (11 - d2 if d2 > 1 else 0))


if __name__ == '__main__':
    verifica_cpf = VerificaCPF('046.65.993-79')
    verifica_cpf.validar()