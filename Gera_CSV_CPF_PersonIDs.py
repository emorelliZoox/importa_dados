# Rotina para gerar uma relação a ser levada ao EC2

import mysql.connector
import pandas as pd
import time

# Conexão (maybe it could be a separated function)
mydb = mysql.connector.connect(
    host="localhost",
    user="morelli",
    passwd="ta4rembo",
    database="pessoas"
)

mycursor = mydb.cursor()

acabou = False

stm = "SELECT cpf, person FROM pessoas "
mycursor.execute(stm)

cargaNum = 1

while not acabou:

    query_result = mycursor.fetchmany(500000)
    start_time = time.time()
    if query_result:
        pessoas = []
        for person in query_result:
            cpf = person[0]
            id = person[1]
            linha = {'cpf': cpf, 'id': id}
            pessoas.append(linha)

        final = pd.DataFrame(pessoas)
        final.to_csv('cpf_id' + str(cargaNum) + '.csv', sep=',', encoding='utf-8', index=False)
        print("Concluiu carga ", str(cargaNum), " em %s segundos." % (time.time() - start_time), flush=True)
        cargaNum = cargaNum + 1
    else:
        acabou = True

print("Fim", flush=True)


