import pandas as pd
import time
import sqlalchemy


def fnc_conecta_servidor (database_username, database_password, database_ip, database_name):
    # Conexão
    database_connection = None
    try:
        database_connection = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                                       format(database_username, database_password,
                                                              database_ip, database_name))
    except Exception as error:
        print(error)

    return database_connection

##################################################
start_time = time.time()

print ("Início: ", time.strftime('%l:%M%S%p %Z on %b %d, %Y'))

caminho = "/home/emorelli/Python/Dados_Conexao/"

database_connection = fnc_conecta_servidor('morelli', 'ta4rembo', 'localhost', 'pessoas')

if database_connection:
    # Lendo os  arquivos (1, número máximo + 1)
    for numArquivo in range(150, 151):
        arquivo = "login_portal" + str(numArquivo) + ".csv"

        print("Lendo arquivo: ", arquivo)

        dados = []

        try:
            f = open(caminho + arquivo, 'r')
        except:
            f = None

        if f:
            data = f.read()

            rows = data[1:].split('\n')
            for r in rows:
                values = r.split(',')
                if values[0]:
                    print ('Person: ', values[7])
                    #company, company_name, created, duration, expires, hotspot, hotspot_name, person
                    linha = {'company_name': values[1], 'created': values[2], 'duration': values[3]
                        , 'expires': values[4], 'person': values[7]}
                    dados.append(linha)

            df = pd.DataFrame(dados, columns=["company_name", "created", "duration", "expires", "person"])
            df.to_sql(con=database_connection, name='conexoes', if_exists='append', index=False)
    print("Concluiu carga em %s segundos." % (time.time() - start_time), flush=True)
else:
    print("Ocorreu erro na conexão. ")