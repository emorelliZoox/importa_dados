# Bibliotecas
# import pandas as pd
import Util


def carrega_documentos():
    # Conexões
    cliente_mongodb_zoox = Util.fnc_Conecta_Base_Documentos('', '', 'localhost', '27017', 'zoox')
    db_clientes = cliente_mongodb_zoox.zoox

    # Preparando destino dos dados:
    documentos = []

    # Leitura documentos e carga dataframe
    for u in db_clientes.clientes.find({"providers.bigdata_corp.relations_data": {"$exists": True},
                                        "providers.bigdata_corp.name": {"$exists": True}
#                                        , "providers.bigdata_corp.name": "Vanda Lucia Pereira"
                                        },
                                       {"_id": 0, "providers.bigdata_corp.name": 1,
                                        "providers.bigdata_corp.relations_data": 1}):
        conexoes = (u["providers"]["bigdata_corp"]["relations_data"])
        for l in conexoes:
            if "relation_name" in l:
                conexao = [
                    u["providers"]["bigdata_corp"]["name"],
                    l["relation_name"],
                    l["relation_type"]
                ]
                documentos.append(conexao)
                print(u["providers"]["bigdata_corp"]["name"], "- Conhecido: ", l["relation_name"],
                      "- Relação: ", l["relation_type"])
            else:
                print (u["providers"]["bigdata_corp"]["name"], " -> nome Ausente!")
    return documentos
# ---------------------------------------------------------------------------------Fim carrega_documentos


def gera_csv(lista_conexoes):
    
    import csv
    print("-----------------------------Descarregando a Lista!")
    f = open('saida.csv', 'w', newline='', encoding='utf-8')
    try:
        writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for l in lista_conexoes:
            print("Nome: ", l[0], "Pessoa relacionada:", l[1], "Conexão: ", l[2])
            writer.writerow([l[0], l[1], l[2]])
        print("-----------------------------Gerado arquivo csv!")
    finally:
        f.close()
# ---------------------------------------------------------------------------------Fim gera_csv


def gera_grafo(lista_conexoes):

    # Conexão Neo4J
    from neo4j.v1 import GraphDatabase, basic_auth
    driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "ta4rembo"))
    session = driver.session()

    import UtilNeo4J
    # Limpeza inicial
    no = session.run("MATCH (p) DETACH DELETE p")

    # Futuramente melhorar esta estrutura de dados. Quem sabe usando um data frame
    nomes_inseridos = []
    id_nos_inseridos = []

    for l in lista_conexoes:
        # Para cada elemento em lista_conexoes:
        #   Se ainda não inseriu l[0], insere nó.
        #   Se ainda não inseriu l[1], insere nó.

        # Futuramente criar uma função para generalizar o tratamento de l[0] e l[1]
        if l[0] not in nomes_inseridos:
            propriedades = "nome:" + "'" + l[0] + "'"
            p = [''.join(propriedades[0:len(propriedades)])]
            id_no1 = UtilNeo4J.fnc_Insere_No(session, "Pessoa", p)
            nomes_inseridos.append(l[0])
            id_nos_inseridos.append(id_no1)
        else:
            id_no1 = nomes_inseridos.index(l[0]) + 1 # Porque o id do grafo começa por 1 e o índice desta lista começa por zero

        if l[1] not in nomes_inseridos:
            propriedades = "nome:" + "'" + l[1] + "'"
            p = [''.join(propriedades[0:len(propriedades)])]
            id_no2 = UtilNeo4J.fnc_Insere_No(session, "Pessoa", p)
            nomes_inseridos.append(l[1])
        else:
            id_no2 = nomes_inseridos.index(l[1]) + 1

        #   Cria aresta entre l[0] e l[1] usando l[2]
        id_aresta = UtilNeo4J.fnc_Insere_Aresta(session, "Pessoa", id_no1, id_no2, "Pessoa", l[2])

    # Fecha conexão
    session.close()
# ---------------------------------------------------------------------------------Fim gera_grafo
    

lista_conexoes = carrega_documentos()

if lista_conexoes:
    gera_csv(lista_conexoes)
    gera_grafo(lista_conexoes)
# Pendência: garantir unicidade de trincas. Por enquanto, não estamos considerando homônimos
