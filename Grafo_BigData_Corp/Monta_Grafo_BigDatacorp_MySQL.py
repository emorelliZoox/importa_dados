# Bibliotecas
# import pandas as pd

import mysql.connector
import UtilNeo4J
import time

def fnc_conecta_servidor_MySQL (database_username, database_password, database_ip, database_name):
    # Conexão
    database_connection = None
    try:
        database_connection = mysql.connector.connect(
                                        host=database_ip,
                                        user=database_username,
                                        passwd=database_password,
                                        database=database_name
)
    except Exception as error:
        print(error)

    return database_connection

##################################################


def gera_grafo():
    # Conexões
    database_connection = fnc_conecta_servidor_MySQL ('morelli', 'ta4rembo', 'localhost', 'grafo')

    acabou = False

    if database_connection:

        from neo4j.v1 import GraphDatabase, basic_auth
        driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "ta4rembo"))
        graph_session = driver.session()

        if graph_session:

            # Limpeza inicial
            no = graph_session.run("MATCH (p) DETACH DELETE p")

            cargaNum = 1
            cpfs_inseridos = []
            id_nos_inseridos = []

            print("Início", flush=True)

            stm = "select p.nome Nome_Pessoa, p.cpf CPF_Pessoa, p.genero, p.obito, \
            r.cpf_relacionado CPF_Relacionado, r.nome Nome_Relacionado, r.relacionamento \
             from pessoas p inner join relacionamentos r  on p.cpf = r.cpf_pessoa "

            mycursor = database_connection.cursor()
            mycursor.execute(stm)

            while not acabou and cargaNum < 2:

                query_result = mycursor.fetchmany(10)
                start_time = time.time()

                if query_result:

                    for p in query_result:
                        nome_pessoa = p[0]
                        cpf_pessoa = p[1]
                        genero = p[2]
                        obito = p[3]
                        cpf_relacionado = p[4]
                        nome_relacionado = p[5]
                        relacionamento = p[6]

                        if cpf_pessoa not in cpfs_inseridos:
                            propriedades = "nome:" + "'" + nome_pessoa + "'" + \
                                           ",cpf:" + "'" + cpf_pessoa + "'" + \
                                           ",genero:" + "'" + genero + "'" + \
                                           ",obito:" + "'" + obito + "'"
                            p = [''.join(propriedades[0:len(propriedades)])]
                            id_no1 = UtilNeo4J.fnc_Insere_No(graph_session, "Pessoa", p)
                            cpfs_inseridos.append(cpf_pessoa)
                            id_nos_inseridos.append(id_no1)
                        else:
                            id_no1 = cpfs_inseridos.index(cpf_pessoa) + 1 # Porque o id do grafo começa por 1 e o índice desta lista começa por zero

                        if cpf_relacionado not in cpfs_inseridos:
                            propriedades = "nome:" + "'" + nome_relacionado + "'" + \
                                           ",cpf:" + "'" + cpf_relacionado + "'"
                            p = [''.join(propriedades[0:len(propriedades)])]
                            id_no2 = UtilNeo4J.fnc_Insere_No(graph_session, "Pessoa", p)
                            cpfs_inseridos.append(cpf_relacionado)
                        else:
                            id_no2 = cpfs_inseridos.index(cpf_relacionado) + 1

                        #   Cria aresta entre cpf_pessoa e cpf_relacionado usando relacionamento
                        id_aresta = UtilNeo4J.fnc_Insere_Aresta(graph_session, "Pessoa", id_no1, id_no2, "Pessoa", relacionamento)
                    print("Concluiu carga ", str(cargaNum), " em %s segundos." % (time.time() - start_time), flush=True)
                    cargaNum = cargaNum + 1
                else:
                    acabou = True
                    graph_session.close()
        else:
            print("Problemas na conexão ao Grafo", flush=True)
    else:
        print("Problemas na conexão ao MySQL", flush=True)


print("Fim", flush=True)
# ---------------------------------------------------------------------------------Fim gera_grafo


gera_grafo()
# Pendência: acrescentar demais atributos (genero e obito) para nós criados a partir de relacionamentos
