# Conexão Neo4J
from neo4j.v1 import GraphDatabase, basic_auth
driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "ta4rembo"))
session = driver.session()

#import UtilNeo4J
# Limpeza inicial
no = session.run("MATCH (p) DETACH DELETE p")