import pandas as pd
import time
import sqlalchemy


def fnc_conecta_servidor (database_username, database_password, database_ip, database_name):
    # Conexão
    database_connection = None
    try:
        database_connection = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                                       format(database_username, database_password,
                                                              database_ip, database_name))
    except Exception as error:
        print(error)

    return database_connection

##################################################
start_time = time.time()

print ("Início: ", time.strftime('%l:%M%S%p %Z on %b %d, %Y'))

arquivo = "/home/emorelli/BigDataCorp/16_01_ZOOX_BDC20190020_Entrega/16_01_ZOOX_BDC20190020_DadosBasicos_.txt"

database_connection = fnc_conecta_servidor('morelli', 'ta4rembo', 'localhost', 'grafo')

if database_connection:

    f = open(arquivo, 'r')

    data = f.read()

    linhas = 0
    dados = []

    rows = data[1:].split('\n')
    for r in rows:
        values = r.split('|')
        print("Vai processar pessoa: ", values)

        if (linhas == 500):
            df_pessoa = pd.DataFrame(dados, columns=["cpf", "nome", "genero", "obito"])
            df_pessoa.to_sql(con=database_connection, name='pessoas', if_exists='append', index=False)
            linhas = 0
            dados = []
        else:
            linhas = linhas + 1
            if (len(values) > 1):
                cpf = values[0]
                nome = values[1]
                genero = values[5]
                obito = values[7]
                linha = {'cpf': cpf, 'nome': nome, 'genero': genero, 'obito': obito}

                if cpf.isnumeric():
                    dados.append(linha)

    print("Concluiu carga em %s segundos." % (time.time() - start_time), flush=True)
else:
    print("Ocorreu erro na conexão. ")