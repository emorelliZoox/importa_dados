from verifica_cpf import VerificaCPF
import pandas as pd
cpfs = pd.read_csv("Conexoes_por_CPF_Mob2Buy.csv")
series_rt = cpfs['cpf']


cpfs_unicos = set([])
for i in series_rt:
    cpfs_unicos.add(i)

print    (len(cpfs_unicos))
tamanhos_invalidos = 0
cpfs_invalidos = 0
conta_tamanhos = {}

for i in cpfs_unicos:
    tamanho = len(str(i))
    if tamanho != 11:
        #print ("Cpf com tamanho inválido", i, " Tamanho: ", len(str(i)))
        tamanhos_invalidos = tamanhos_invalidos + 1
        if tamanho in conta_tamanhos:
            conta_tamanhos[tamanho] = conta_tamanhos[tamanho] + 1
        else:
            conta_tamanhos[tamanho] = 1
    print ('CPF :', str(i))
    verifica_cpf = VerificaCPF(str(i).rjust(11, '0'))
    if not verifica_cpf.validar():
        cpfs_invalidos = cpfs_invalidos + 1

print ("Tamanhos Invalidos: ", tamanhos_invalidos)
print (conta_tamanhos)
print ("CPFs Invalidos: ", cpfs_invalidos)

