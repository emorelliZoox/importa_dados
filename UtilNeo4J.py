# Módulo de utilitários Neo4J. A ideia seria reunir aqui funções utilizadas por mais de um outro módulo
def fnc_Retorna_Proximo_Max_Id_PUC (session):
    # Retorna último Id_PUC utilizado em nós ou arestas
    max_id_no_puc = session.run('MATCH (m) RETURN MAX(m.id_puc) as MaxID')

    # Melhoria futura: descobrir método da classe BoltStatementResult retornando conteúdo de atributo _keys
    for n in max_id_no_puc:  # somente haverá um registro, pois trata-se de uma consulta de agregação
        max_no = n["MaxID"]
        if not max_no:
            max_no = 0  # Grafo vazio!

    # Retorna último Id_PUC utilizado em arestas
    max_id_arco_puc = session.run('MATCH ()-[r]->() RETURN MAX(r.id_puc) as MaxID')
    for r in max_id_arco_puc:  # somente haverá um registro, pois trata-se de uma consulta de agregação
        max_arco = r["MaxID"]
        if not max_arco:
            max_arco = 0  # Nenhuma aresta!

    # Retorna maior dos dois números
    max_id_puc = max_no + 1
    if max_id_puc <= max_arco:
        max_id_puc = max_arco + 1

    return max_id_puc
# Fim  fnc_Retorna_Proximo_Max_Id_PUC

def func_Busca_No (session, rotulo, identificacao):
	# Caso sejam encontrados mais de um nó ou caso não apareça nenhum, retornamos None
    id_PUC_retorno = None
    propriedade_identificadora = '{'
    q = identificacao.split(":")
    q = [x.strip(' ') for x in q]  # removendo eventuais espaços
    propriedade_identificadora += q[0] + ":" + q[1] + "'" + '}) '
    s = 'MATCH (m: ' + rotulo + propriedade_identificadora + 'RETURN m.id_puc AS id_puc'
    no = session.run(s)
    lista_id_PUC = []
    for r in no:  # a cada varrida encontra, no máximo, um nó pois estamos utilizando uma propriedade identificadora
        lista_id_PUC.append(r["id_puc"])
    nos_encontrados =  len(lista_id_PUC)

    if nos_encontrados == 1:  # Significa que achou 1 nó
        id_PUC_retorno = lista_id_PUC[0]

    return id_PUC_retorno
 #Fim func_Busca_No

def func_Busca_Arco (session, id_PUC_origem, id_PUC_destino):
    id_PUC_retorno = False
    arco_encontrado = None
    s = 'MATCH (o {id_puc:' + str(id_PUC_origem) + '})-[r]-(d {id_puc:' + str(id_PUC_destino) + '})RETURN r.id_puc AS id_puc'
    no = session.run(s)
    for r in no:  # a cada varrida encontra, no máximo, um nó pois estamos utilizando uma propriedade identificadora
        arco_encontrado = r["id_puc"]

    if arco_encontrado:  # Significa que não achou 1 nó
        id_PUC_retorno = True

    return id_PUC_retorno
 #Fim func_Busca_Arco

def fnc_Insere_No(session, rotulo, propriedades):
    # Grava um nó no Grafo
    # Retorna último Id_PUC utilizado em nós ou arestas
    max_id_puc = fnc_Retorna_Proximo_Max_Id_PUC(session)
    s = "CREATE (n:" + rotulo + " {id_puc : " + str(max_id_puc)
    for p in propriedades:          # Eliminando a primeira posição
        s = s + ', ' + p #+ "'"
    # Fechando a instrução
    s = s + "})"
    s = s.replace (", '", "")
    print("INSERÇÃO: " + s)
    # Execução
    session.run(s)
    return max_id_puc
# Fim  fnc_Insere_No

def fnc_Atualiza_No(session, rotulo, propriedades, propriedades_identificadoras, clausula_lista_propriedades_identificadoras):
    # Encontrado um nó, atualiza suas propriedades com novos valores lidos
    demais_propriedades = []
    for p in propriedades:
        if p not in propriedades_identificadoras:
            demais_propriedades.append(p)
    if demais_propriedades: # Vale a pena colocar na Caixa de Areia quando não houver propriedades?
    # Montando a cláusula SET
        clausula_SET = 'SET '
        for d in demais_propriedades:
            if d:               # Para evitar campo vazio
                campos = d.split(':')
                clausula_SET +=  'm.' + campos[0] + ' = ' + '"' + campos[1] + '"' + ','
        clausula_SET = clausula_SET[:-1]    # Retira última vírgula
    # Monta comando:
        s = 'MATCH (m: ' + rotulo + clausula_lista_propriedades_identificadoras + clausula_SET + ' RETURN m'
        print("ATUALIZAÇÃO: " + s)
        no = session.run(s)
    return None
# Fim  fnc_Atualiza_No

def fnc_Insere_Aresta(session, rotulo_origem, id_PUC_origem, id_PUC_destino, rotulo_destino, nome_arco):
    # Grava uma aresta no Grafo
    # Gera próximo id_puc
    novo_id_puc = fnc_Retorna_Proximo_Max_Id_PUC(session)
    s = "MATCH (o: " + rotulo_origem + "{id_puc:" + str(id_PUC_origem) + "}),(d: " + rotulo_destino + "{id_puc:" + str(
        id_PUC_destino) + "}) CREATE (o) -[a:" + nome_arco + "{id_puc: " + str(novo_id_puc) + "}]->(d)"
    # Cria arco
    print("CRIA ARESTA: " + s)
    session.run(s)
    return novo_id_puc
# Fim  fnc_Insere_No

# PENDÊNCIAS
#
#  1) Ao atualizar um nó, fazer o MATCH buscar pelo id_PUC, e não pelas propriedades identificadoras


