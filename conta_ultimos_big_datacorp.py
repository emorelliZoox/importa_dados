# Rotina para contar quantos registros houve com provider bigdatacorp preenchido
# Só considerar aqui quem tem cpf nulo e email NÃO NULO

# Critérios para buscar dados (ver no arquivo exporta_para_big_datacorp.py)

import mysql.connector
import jsons
import pandas as pd
import time


start_time = time.time()
# Conexão (maybe it could be a separated function)
mydb = mysql.connector.connect(
    host="zooxwifi-rr1.cf7xmzoyfi3z.us-east-1.rds.amazonaws.com",
    user="consulta",
    passwd="OzgRn^Uh85",
    database="person"
)
mycursor = mydb.cursor()

acabou = False

print ("Início", flush=True)

stm = "SELECT providers, updated, id FROM person where updated >= 20181201 and updated <= 20190110"
mycursor.execute(stm)
cargaNum = 1
qtos = 0

while not acabou and cargaNum < 10:

    query_result = mycursor.fetchmany(5000)

    print("Concluiu busca em %s segundos." % (time.time() - start_time), flush=True )
    start_time = time.time()

    if query_result:
        for person in query_result:
            providers = person[0]
            if providers:
                loaded_json = jsons.loads(providers)
                for y in loaded_json:
                    if (y== "bigdata_corp"):
                        qtos = qtos + 1
                        #print(person[1], "-", person[2])

        print("Valor corrente", str(qtos), "Concluiu carga ", str(cargaNum), " em %s segundos." % (time.time() - start_time), flush=True)
        cargaNum = cargaNum + 1
    else:
        acabou = True

print ("Quantos", qtos, flush=True)
print ("Fim", flush=True)