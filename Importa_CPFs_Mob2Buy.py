import pandas as pd
import time
import sqlalchemy


def fnc_conecta_servidor (database_username, database_password, database_ip, database_name):
    # Conexão
    database_connection = None
    try:
        database_connection = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                                       format(database_username, database_password,
                                                              database_ip, database_name))
    except Exception as error:
        print(error)

    return database_connection

##################################################
start_time = time.time()

print ("Início: ", time.strftime('%l:%M%S%p %Z on %b %d, %Y'))

caminho = "/home/emorelli/Python/Dados_Mob2Buy/"

database_connection = fnc_conecta_servidor('morelli', 'ta4rembo', 'localhost', 'pessoas')

if database_connection:
    # Lendo os  arquivos (1, número máximo + 1)
    for numArquivo in range(11, 706):
        arquivo = "pessoas_mob2buy" + str(numArquivo) + ".csv"

        print("Lendo arquivo: ", arquivo)

        dados = []

        try:
            f = open(caminho + arquivo, 'r')
        except:
            f = None

        if f:
            data = f.read()

            rows = data[1:].split('\n')
            for r in rows:
                values = r.split(',')
                if values[0]:
                    #print ('Person: ', values[2])
                    #cpf, email, id, name
                    linha = {'cpf': values[0], 'email': values[1], 'person': values[2]
                        , 'nome': values[3]}
                    dados.append(linha)

            df = pd.DataFrame(dados, columns=["cpf", "email", "person", "nome"])
            try:
                df.to_sql(con=database_connection, name='pessoas', if_exists='append', index=False)
            except:
                print ("Falhou a carga do arquivo: ", arquivo)
    print("Concluiu carga em %s segundos." % (time.time() - start_time), flush=True)
else:
    print("Ocorreu erro na conexão. ")