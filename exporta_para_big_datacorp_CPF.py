# Rotina para envio de dados à Big Datacorp. Prioridade: cpf, nome, data de nascimento
# Critério (obter o primeiro não nulo):
# 1) Ordem para CPF:
#       cpf,
#       providers.zooxwifi.cpf,
#       providers.bigdatacorp.cpf,
#       providers.fnrh.cpf,
#       providers.ipiranga_kmv.cpf
# Se não estiver preenchido em nenhuma das possibilidades acima, descartar o registro
# 2) Ordem para nome:
#       name,
#       providers.zooxwifi.name,
#       providers.facebook.name,
#       providers.bigdatacorp.name,
#       providers.instagram.name,
#       providers.twitter.name,
#       providers.unisuam.name,
#       providers.linkedin.name,
#       providers.fnrh.name,
#       providers.ipiranga_kmv.name
# Se não estiver preenchido em nenhuma das possibilidades acima, deixar resposta vazia
# 3) Ordem para email:
#       email,
#       providers.zooxwifi.email,
#       providers.facebook.email,
#       providers.bigdatacorp.email,
#       providers.linkedin.email,
#       providers.ipiranga_kmv.email
# Se não estiver preenchido em nenhuma das possibilidades acima, deixar resposta vazia

import mysql.connector
import jsons
import pandas as pd
import time

def fnc_fill_attribute (attr_name, order_list, key_list):
    return_value = None
    for order in order_list:
        if not return_value:
            for l in key_list:
                if (l["provider"] == order):
                    try:
                        return_value = l[attr_name]
                    except:
                        pass
    return return_value


start_time = time.time()

# Conexão (maybe it could be a separated function)
mydb = mysql.connector.connect(
    host="labsteste.cf7xmzoyfi3z.us-east-1.rds.amazonaws.com",
    user="consulta",
    passwd="OzgRn^Uh85",
    database="person"
)
mycursor = mydb.cursor()

pageSize = 50
pageNum = 1

acabou = False

cpf_order_list = ['zooxwifi', 'bigdata_corp', 'fnrh', 'ipiranga_kmv' ]
name_order_list = ['zooxwifi', 'facebook', 'bigdata_corp', 'instagram', 'twitter', 'unisuam', 'linkedin', 'fnrh', 'ipiranga_kmv' ]
email_order_list = ['zooxwifi', 'facebook', 'bigdata_corp', 'linkedin', 'ipiranga_kmv' ]

print ("Início", flush=True)
while not acabou and pageNum < 2:

    stm = "SELECT cpf, name, email, providers FROM person order by created " \
          "limit " + str(((pageNum - 1) * pageSize)) + ',' + str(pageSize)
    mycursor.execute(stm)

    query_result = mycursor.fetchall()
    print("Concluiu conexão  em %s segundos." % (time.time() - start_time), flush=True )
    start_time = time.time()

    if query_result:
        pessoas = []
        for person in query_result:
            cpf = person[0]
            name = person[1]
            email = person[2]

            # Handle non empty providers
            providers = person[3]
            if providers:
                loaded_json = jsons.loads(providers)
                keys = []
                for y in loaded_json:
                    provider_keys = {}
                    provider_keys["provider"] = y
                    provider_details = loaded_json[y]

                    if not name:
                        try:
                            name_key = provider_details["name"]
                        except:
                            name_key = None
                        if name_key:
                            provider_keys["name"] = name_key

                    if not cpf:
                        try:
                            cpf_key = provider_details["cpf"]
                        except:
                            cpf_key = None
                        if cpf_key:
                            provider_keys["cpf"] = cpf_key

                    if not email:
                        try:
                            email_key = provider_details["email"]
                        except:
                            email_key = None
                        if email_key:
                            provider_keys["email"] = email_key

                    keys.append(provider_keys)

                    if not name:
                        name = fnc_fill_attribute("name", name_order_list, keys)

                    if not cpf:
                        cpf = fnc_fill_attribute("cpf", cpf_order_list, keys)

                    if not email:
                        name = fnc_fill_attribute("email", email_order_list, keys)

                    if cpf: # Having just cpf, it is enough
                        linha = { 'cpf' : cpf, 'name': name, 'email': email }
                        pessoas.append (linha)
                        break # Once complete information is loaded, there is no point in going on with the loop
        final = pd.DataFrame(pessoas)
        final.to_csv('pessoas' + str(pageNum) + '.csv', sep=',', encoding='utf-8', index = False)
        print("Concluiu carga ", str(pageNum), " em %s segundos." % (time.time() - start_time), flush=True)
        pageNum = pageNum + 1
    else:
        acabou = True
print ("Fim", flush=True)
