import pandas as pd
import time
import sqlalchemy


def fnc_conecta_servidor (database_username, database_password, database_ip, database_name):
    # Conexão
    database_connection = None
    try:
        database_connection = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                                       format(database_username, database_password,
                                                              database_ip, database_name))
    except Exception as error:
        print(error)

    return database_connection

##################################################
start_time = time.time()

print ("Início: ", time.strftime('%l:%M%S%p %Z on %b %d, %Y'))

caminho = "/home/emorelli/Python/Dados/"
qtde_cpfs_unicos = 0

database_connection = fnc_conecta_servidor('morelli', 'ta4rembo', 'localhost', 'cpfs')

if database_connection:
    # Lendo os 125 arquivos (1,126)
    for numArquivo in range(1, 126):
        arquivo = "pessoas" + str(numArquivo) + ".csv"

        cpfs_unicos = set([])

        f = open(caminho + arquivo, 'r')

        data = f.read()

        rows = data[1:].split('\n')
        for r in rows:
            values = r.split(',')
            cpfs_unicos.add(values[0])

        qtde_cpfs_unicos = qtde_cpfs_unicos + len (cpfs_unicos)

        df_cpf = pd.DataFrame(list(cpfs_unicos), columns=["cpf"])
        df_cpf.to_sql(con=database_connection, name='cpfs', if_exists='append', index=False)

        print ("Gravou arquivo: ", numArquivo)

    print ("CPFs únicos: ", qtde_cpfs_unicos)

    print("Concluiu carga em %s segundos." % (time.time() - start_time), flush=True)
else:
    print("Ocorreu erro na conexão. ")