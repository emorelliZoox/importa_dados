# Rotina semelhante à exporta_para_big_datacorp (veja sua documentação)
# Só considerar aqui quem tem cpf não nulo, mas utilizando fetchmany ao invés de fetchall

# Critérios para buscar dados (ver no arquivo exporta_para_big_datacorp.py)

import mysql.connector
import jsons
import pandas as pd
import time

def fnc_fill_attribute (attr_name, order_list, key_list):
    return_value = None
    for order in order_list:
        if not return_value:
            for l in key_list:
                if (l["provider"] == order):
                    try:
                        return_value = l[attr_name]
                    except:
                        pass
    return return_value


start_time = time.time()
# Conexão (maybe it could be a separated function)
mydb = mysql.connector.connect(
    host="zooxwifi-rr1.cf7xmzoyfi3z.us-east-1.rds.amazonaws.com",
    user="consulta",
    passwd="OzgRn^Uh85",
    database="person"
)
mycursor = mydb.cursor()

acabou = False

cpf_order_list = ['zooxwifi', 'bigdata_corp', 'fnrh', 'ipiranga_kmv' ]
name_order_list = ['zooxwifi', 'facebook', 'bigdata_corp', 'instagram', 'twitter', 'unisuam', 'linkedin', 'fnrh', 'ipiranga_kmv' ]
email_order_list = ['zooxwifi', 'facebook', 'bigdata_corp', 'linkedin', 'ipiranga_kmv' ]

print ("Início", flush=True)

stm = "SELECT id, cpf, name, email, providers FROM person "
mycursor.execute(stm)
cargaNum = 1

while not acabou and cargaNum < 2:

    query_result = mycursor.fetchmany(10)

    print("Concluiu busca em %s segundos." % (time.time() - start_time), flush=True )
    start_time = time.time()

    if query_result:
        pessoas = []
        for person in query_result:
            id = person[0] # we assume it will never be empty, as it is a unique key
            cpf = person[1]
            name = person[2]
            email = person[3]

            # Handle non empty providers
            providers = person[4]
            if providers:
                loaded_json = jsons.loads(providers)
                keys = []
                for y in loaded_json:
                    provider_keys = {}
                    provider_keys["provider"] = y
                    provider_details = loaded_json[y]

                    if not name:
                        try:
                            name_key = provider_details["name"]
                        except:
                            name_key = None
                        if name_key:
                            provider_keys["name"] = name_key

                    if not cpf:
                        try:
                            cpf_key = provider_details["cpf"]
                        except:
                            cpf_key = None
                        if cpf_key:
                            provider_keys["cpf"] = cpf_key

                    if not email:
                        try:
                            email_key = provider_details["email"]
                        except:
                            email_key = None
                        if email_key:
                            provider_keys["email"] = email_key

                    keys.append(provider_keys)

                    if not name:
                        name = fnc_fill_attribute("name", name_order_list, keys)

                    if not cpf:
                        cpf = fnc_fill_attribute("cpf", cpf_order_list, keys)

                    if not email:
                        name = fnc_fill_attribute("email", email_order_list, keys)

                    if cpf:  # Having just cpf, it is enough
                       linha = { 'id' : id, 'cpf' : cpf, 'name': name, 'email': email }
                       pessoas.append (linha)
                       break # Once complete information is loaded, there is no point in going on with the loop
        final = pd.DataFrame(pessoas)
        final.to_csv('pessoas_mob2buy' + str(cargaNum) + '.csv', sep=',', encoding='utf-8', index = False)
        print("Concluiu carga ", str(cargaNum), " em %s segundos." % (time.time() - start_time), flush=True)
        cargaNum = cargaNum + 1
    else:
        acabou = True

print ("Fim", flush=True)