# Com base em uma lista de IDs de pessoas, recuperar seus emails gerando uma lista
#

import mysql.connector
import jsons
import pandas as pd
import time

def fnc_fill_attribute (attr_name, order_list, key_list):
    return_value = None
    for order in order_list:
        if not return_value:
            for l in key_list:
                if (l["provider"] == order):
                    try:
                        return_value = l[attr_name]
                    except:
                        pass
    return return_value

#################################################################################################
start_time = time.time()
# Conexão (maybe it could be a separated function)
mydb = mysql.connector.connect(
    host="zooxwifi-rr1.cf7xmzoyfi3z.us-east-1.rds.amazonaws.com",
    user="consulta",
    passwd="OzgRn^Uh85",
    database="person"
)
mycursor = mydb.cursor()

acabou = False

email_order_list = ['zooxwifi', 'facebook', 'bigdata_corp', 'linkedin', 'ipiranga_kmv' ]

print ("Início", flush=True)

# Carrega Ids
lista_id = []
arquivo = "/home/emorelli/Python/ImportaCPFs/ShoppingReforma222/person_id.csv"
try:
    f = open(arquivo, 'r')
except:
    f = None

if f:
    data = f.read()

rows = data[1:].split('\n')
for r in rows:
    lista_id.append(r)

print ("Tamanho da lista", len(lista_id))

stm = "SELECT id, email, providers FROM person "
mycursor.execute(stm)
cargaNum = 1

while not acabou and cargaNum < 2:

    query_result = mycursor.fetchmany(10000)

    print("Concluiu busca em %s segundos." % (time.time() - start_time), flush=True )
    start_time = time.time()

    if query_result:
        pessoas = []
        for person in query_result:
            id = person[0] # we assume it will never be empty, as it is a unique key
            email = person[1]

            # Handle non empty providers
            providers = person[2]
            if providers:
                loaded_json = jsons.loads(providers)
                keys = []
                for y in loaded_json:
                    provider_keys = {}
                    provider_keys["provider"] = y
                    provider_details = loaded_json[y]

                    if not email:
                        try:
                            email_key = provider_details["email"]
                        except:
                            email_key = None
                        if email_key:
                            provider_keys["email"] = email_key

                    keys.append(provider_keys)

                    if not email:
                        name = fnc_fill_attribute("email", email_order_list, keys)

                    if email:
                        if id in lista_id:
                            linha = {'email': email }
                            pessoas.append (linha)
                            #print (id)
                            break # Once complete information is loaded, there is no point in going on with the loop
        final = pd.DataFrame(pessoas)
        final.to_csv('emails_' + str(cargaNum) + '.csv', sep=',', encoding='utf-8', index = False)
        print("Concluiu carga ", str(cargaNum), " em %s segundos." % (time.time() - start_time), flush=True)
        cargaNum = cargaNum + 1
    else:
        acabou = True

print ("Fim", flush=True)