# Rotina para gerar uma relação a ser levada ao EC2

import mysql.connector
import csv
import pandas

# Conexão (maybe it could be a separated function)
mydb = mysql.connector.connect(
    host="localhost",
    user="morelli",
    passwd="ta4rembo",
    database="cpfs"
)

mycursor = mydb.cursor()

stm = "SELECT cpf FROM cpfs_validos "
mycursor.execute(stm)

query_result = mycursor.fetchall()

list_cpfs = []

for c in query_result:
    if bool(c[0].strip()):
        list_cpfs.append(c[0])

print ("Lista Montada", flush=True)

df = pandas.DataFrame(data={"cpf": list_cpfs})
df.to_csv("./cpfs_validos.csv", sep=',',index=False)

print ("Fim", flush=True)



